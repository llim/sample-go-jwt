package main

import (
	_ "go-testground/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}

