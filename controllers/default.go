package controllers

import (
	"crypto/ecdsa"
	"fmt"
	"io/ioutil"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/dgrijalva/jwt-go"
	"github.com/garyburd/redigo/redis"
	"github.com/twinj/uuid"
)

// MainController is the main controller
type MainController struct {
	beego.Controller
}

// MyCustomClaims custom structure
type MyCustomClaims struct {
	Foo string `json:"foo"`
	jwt.StandardClaims
}

//Post handler the POST request
func (c *MainController) Post() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"
	c.TplName = "index.tpl"

	logger := logs.NewLogger(10000)
	logger.SetLogger("console")

	tokenString := c.GetString("token")

	key, err := ioutil.ReadFile("conf/ec-public.pem")

	if err != nil {
		logger.Error("Failed to read private key: %v", err)
		c.Abort("500")
	}

	var ecdsaKey *ecdsa.PublicKey
	ecdsaKey, err = jwt.ParseECPublicKeyFromPEM(key)
	if err != nil {
		logger.Error("Unable to parse ECDSA private key: %v", err)
		c.Abort("500")
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodECDSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return ecdsaKey, nil
	})

	if token.Valid {
		logger.Info("Token is valid")
		if claims, ok := token.Claims.(jwt.MapClaims); ok {
			logger.Info("foo, nbf %v, %v", claims["foo"], claims["nbf"])
			c.Data["foo"] = claims["foo"]
			c.Data["nbf"] = claims["nbf"]
			c.Data["exp"] = claims["exp"]
			c.Data["jti"] = claims["jti"]
			c.Data["iat"] = claims["iat"]

			redisConn, err := redis.Dial("tcp", "localhost:6379")

			if err != nil {
				logger.Error("Redis error: %v", err)
				c.Abort("500")
			}

			defer redisConn.Close()

			// using Redis HyperLogLog to track uniq token usage
			uuidCheck, err := redis.Int(redisConn.Do("PFADD", "SQTRACK", claims["jti"]))

			if err != nil {
				logger.Error("Redis error: %v", err)
				c.Abort("500")
			}

			// uuidCheck = 0 if not added to HyperLogLog tracking data set, which mean already in the data
			if uuidCheck < 1 {
				logger.Error("Error, UUID existed in HLL data")
				c.Abort("400")
			}
		}

	} else {
		ve, _ := err.(*jwt.ValidationError)

		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			logger.Error("That's not even a token: %v", err)
		} else if ve.Errors&jwt.ValidationErrorExpired != 0 {
			logger.Error("token expired: %v", err)
		} else if ve.Errors&jwt.ValidationErrorNotValidYet != 0 {
			// Token is either expired or not active yet
			logger.Error("token not active: %v", err)
		} else {
			logger.Error("Couldn't handle this token:: %v", err)
		}

		c.Abort("400")
	}
}

//Get handler the GET request
func (c *MainController) Get() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"
	c.TplName = "index.tpl"

	logger := logs.NewLogger(10000)
	logger.SetLogger("console")

	key, err := ioutil.ReadFile("conf/ec-private.pem")

	if err != nil {
		logger.Error("Failed to read private key: %v", err)
		c.Abort("500")
	}

	var ecdsaKey *ecdsa.PrivateKey
	ecdsaKey, err = jwt.ParseECPrivateKeyFromPEM(key)
	if err != nil {
		logger.Error("Unable to parse ECDSA private key: %v", err)
		c.Abort("500")
	}

	// Create a new token object, specifying signing method and the claims
	// you would like it to contain.
	logger.Info("generating token")
	claims := MyCustomClaims{
		"bar",
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Minute * time.Duration(15)).Unix(),
			Issuer:    "MMMMMMM",
			Id:        uuid.NewV4().String(),
			IssuedAt:  time.Now().Unix(),
			NotBefore: time.Now().Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodES256, claims)
	logger.Info("finished generating token")

	// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString(ecdsaKey)

	if err != nil {
		logger.Error("Error: %v", err)
		c.Abort("500")
	}

	c.Data["token"] = tokenString
}
